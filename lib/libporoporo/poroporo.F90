!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

SUBROUTINE poroporo(Nt_loc,J_array_loc,myrank)
  USE m_phys  
  USE m_num
  USE m_source
  USE m_sismo
  IMPLICIT NONE 

  INTEGER :: Nt_loc,myrank
  INTEGER, DIMENSION(Nt) :: J_array_loc

!!! Computation of the incident wave
  ALLOCATE(Ux(1:nx,1:nt))
  ALLOCATE(Uy(1:nx,1:nt))
  ALLOCATE(Uz(1:nx,1:nt))
  Ux=0.d0
  Uy=0.d0
  Uz=0.d0
  IF (type_medium.EQ.1) THEN
!!!Infinite Medium
     IF (P1(1,1)*f1.NE.0D0) THEN
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Computation of the incident Pf wave'
        END IF
        CALL sub_incid_pp_f(Nt_loc,J_array_loc)
     END IF
     IF (P1(1,2)*f2.NE.0D0) THEN
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Computation of the incident Ps wave'
        END IF
        CALL sub_incid_pp_s(Nt_loc,J_array_loc)
     END IF
  ELSEIF (type_medium.EQ.2) THEN
!!!Free Surface
     IF (z.GE.0d0) THEN
        IF (P1(1,1)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the incident Pf wave'
           END IF
           CALL sub_incid_pp_f(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,2)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the incident Ps wave'
           END IF
           CALL sub_incid_pp_s(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,1)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PfPfwave'
           END IF
           CALL sub_reflexff_free_pp(Nt_loc,J_array_loc)
        END IF
        IF (f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PfSwave'
           END IF
           CALL sub_reflexfpsi_free_pp(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,2)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PfPswave'
           END IF
           CALL sub_reflexfs_free_pp(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,1)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PsPfwave'
           END IF
           CALL sub_reflexsf_free_pp(Nt_loc,J_array_loc)
        END IF
        IF (f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PsSwave'
           END IF
           CALL sub_reflexspsi_free_pp(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,2)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PsPswave'
           END IF
           CALL sub_reflexss_free_pp(Nt_loc,J_array_loc)
        END IF
     ELSE
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Error, z can not be smaller than 0 in this configuration'
        END IF
        STOP
     END IF
!!!Wall Boundary
  ELSEIF (type_medium.EQ.3) THEN
     IF (z.GE.0d0) THEN
        IF (P1(1,1)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the incident Pf wave'
           END IF
           CALL sub_incid_pp_f(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,2)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the incident Ps wave'
           END IF
                    CALL sub_incid_pp_s(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,1)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PfPfwave'
           END IF
           CALL sub_reflexff_wall_pp(Nt_loc,J_array_loc)
        END IF
        IF (f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PfSwave'
           END IF
          CALL sub_reflexfpsi_wall_pp(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,2)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PfPswave'
           END IF
           CALL sub_reflexfs_wall_pp(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,1)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PsPfwave'
           END IF
                      CALL sub_reflexsf_wall_pp(Nt_loc,J_array_loc)
        END IF
        IF (f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PsSwave'
           END IF
                      CALL sub_reflexspsi_wall_pp(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,2)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PsPswave'
           END IF
                      CALL sub_reflexss_wall_pp(Nt_loc,J_array_loc)
        END IF
     ELSE
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Error, z can not be smaller than 0 in this configuration'
        END IF
        STOP
     END IF
  ELSEIF (type_medium.EQ.4) THEN
     IF (z.GE.0d0) THEN
        IF (P1(1,1)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the incident Pf wave'
           END IF
           CALL sub_incid_pp_f(Nt_loc,J_array_loc)
           FLUSH(6)
        END IF
        IF (P1(1,2)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the incident Ps wave'
              FLUSH(6)
           END IF
           CALL sub_incid_pp_s(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,1)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PfPfwave'
              FLUSH(6)
           END IF
           CALL sub_reflexff_pp(Nt_loc,J_array_loc)
        END IF
        IF (f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PfSwave'
              FLUSH(6)
           END IF
           CALL sub_reflexfpsi_pp(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,2)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PfPswave'
              FLUSH(6)
           END IF
           CALL sub_reflexfs_pp(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,1)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PsPfwave'
              FLUSH(6)
           END IF
           CALL sub_reflexsf_pp(Nt_loc,J_array_loc)
        END IF
        IF (f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PsSwave'
              FLUSH(6)
           END IF
           CALL sub_reflexspsi_pp(Nt_loc,J_array_loc)
        END IF
        IF (P1(1,2)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the reflected PsPswave'
              FLUSH(6)
           END IF
           CALL sub_reflexss_pp(Nt_loc,J_array_loc)
        END IF
     ELSE
        IF (P2(1,1)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the transmitted PfPfwave'
              FLUSH(6)
           END IF
           CALL sub_transmitff_pp(Nt_loc,J_array_loc)
        END IF
        IF (f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the transmitted PfSwave'
              FLUSH(6)
           END IF
           CALL sub_transmitfpsi_pp(Nt_loc,J_array_loc)
        END IF
        IF (P2(1,2)*f1.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the transmitted PfPswave'
              FLUSH(6)
           END IF
           CALL sub_transmitfs_pp(Nt_loc,J_array_loc)
        END IF
        IF (P2(1,1)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the transmitted PsPfwave'
              FLUSH(6)
           END IF
           CALL sub_transmitsf_pp(Nt_loc,J_array_loc)
        END IF
        IF (f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the transmitted PsSwave'
              FLUSH(6)
           END IF
           CALL sub_transmitspsi_pp(Nt_loc,J_array_loc)
        END IF
        IF (P2(1,2)*f2.NE.0D0) THEN
           IF (myrank.EQ.0) THEN
              WRITE(6,*) 'Computation of the transmitted PsPswave'
              FLUSH(6)
           END IF
           CALL sub_transmitss_pp(Nt_loc,J_array_loc)
        END IF
     END IF
  END IF
END SUBROUTINE poroporo
