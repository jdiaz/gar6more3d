!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

SUBROUTINE sub_reflexss_free_pp(Nt_loc,J_array_loc)
  Use m_phys  
  Use m_num
  Use m_source
  Use m_sismo
  Use m_const
  implicit none 
#include "m_interface.h90"
  integer I,J,K,L
  real*8 t0,r,x,y,ddt,src,t,tau,thead,F
  real*8 :: thead2,deta,dzeta,dq,q,Vs1q,eta,zeta,q0,q1,uxinter,uzinter,uxinterbis,uzinterbis,tau0
  complex*16::pp,Coeff(3),ks1

  integer :: Nt_loc
  integer, dimension(Nt) :: J_array_loc
  integer :: J_loc

  F=P1(1,2)*f2
  deta=pi/(2.*Nqint);
  dzeta=1./Nqint;
  vmax=max(max(Vf1,Vs1),Vpsi1)
  DO I=1,Nx
     x=X1+(I-1)*dx
     y=Y1+(I-1)*dy
     x=sqrt(x**2+y**2)
     r=dsqrt(x**2+(z+h)**2)
     t0=r/Vs1
     DO J_loc=1,Nt_loc
        J = J_array_loc(J_loc)
       t=T1+(j-1)*dt
       uxinter=0.;uzinter=0.;
       if (abs(x/r).gt.Vs1/Vmax) then
          !Computation of the head wave'
          thead=abs(z+h)*sqrt(1/Vs1**2-1/Vmax**2)+abs(x)/Vmax;
          if(z+h.gt.0.) then
             thead2=R/Vs1*(sqrt(1.+(x**2-r**2*Vs1**2/Vmax**2)/(z+h)**2))
          else
             thead2=R/Vs1
          end if
          if((t.gt.thead).and.(t-2.*tdelay.lt.t0)) then
             if (t.ge.t0) then
                if (t-2.*tdelay.gt.thead) then 
                   ddt=(t0-t+2.*tdelay)/Nint 
                   tau0=t-2.*tdelay 
                else 
                   ddt=(t0-thead)/Nint 
                   tau0=thead 
                end if
             else
                if (t-2.*tdelay.gt.thead) then 
                   tau0=t-2.*tdelay 
                   ddt=2.*tdelay/Nint 
                else 
                   ddt=(t-thead)/Nint 
                   tau0=thead 
                end if 
             end if
                DO  k=1,Nint-1
                tau=tau0+(k-1)*ddt+ddt/2.
                q0=sqrt(1/Vs1**2-tau**2/r**2)
                q1=sqrt((tau-(z+h)*sqrt(1./Vs1**2-1./Vmax**2))**2/(x**2)-1/Vmax**2)
                zeta=0.
                src=source(t-tau)
                uxinterbis=0.;uzinterbis=0.
                DO l=1,Nqint
                   eta=asinh(q1/q0)*zeta
                   q=sinh(eta)*q0
                   Vs1q=Vs1/dsqrt(1+Vs1**2*q**2);
                   pp=-ima*(abs(z+h)/r*sqrt(abs(1/Vs1q**2-tau**2/r**2))-abs(x)*tau/r**2)
                   ks1=zsqrt(pp**2+1/Vs1q**2)
                   Coeff=calccoeffs_free_pp(pp,q)
                 uxinterbis=uxinterbis+aimag(ima*pp*Coeff(2)*ks1)
                 uzinterbis=uzinterbis+aimag(ks1**2*Coeff(2))
                 zeta=zeta+dzeta
              END DO
                   uxinter=uxinter+uxinterbis*asinh(q1/q0)*dzeta*ddt*src
                   uzinter=uzinter+uzinterbis*asinh(q1/q0)*dzeta*ddt*src
                end DO
             end if
          if((t.ge.t0).and.(t-2.*tdelay.lt.thead2)) then
             if (t.ge.thead2) then
                if (t-2.*tdelay.gt.t0) then 
                   tau0=t-2.*tdelay 
                   ddt=(thead2-tau0)/Nint 
                else 
                   tau0=t0
                   ddt=(thead2-tau0)/Nint 
                end if 
             else
               if (t-2.*tdelay.gt.t0) then  
                   tau0=t-2.*tdelay  
                   ddt=2.*tdelay/Nint  
                else  
                   ddt=(t-t0)/Nint  
                   tau0=t0 
                end if  
             end if
            DO  k=1,Nint
                tau=tau0+(k-1)*ddt+ddt/2.
                q0=sqrt(tau**2/r**2-1/Vs1**2)
                q1=sqrt((tau-(z+h)*sqrt(1./Vs1**2-1./Vmax**2))**2/(x**2)-1/Vmax**2)
                zeta=0.
                src=source(t-tau)
                uxinterbis=0.;uzinterbis=0.;
                DO l=1,Nqint
                   zeta=zeta+dzeta;
                   eta=acosh(q1/q0)*zeta;
                   q=cosh(eta)*q0;
                   Vs1q=Vs1/dsqrt(1+Vs1**2*q**2);
                   pp=-ima*(abs(z+h)/r*sqrt(abs(1/Vs1q**2-tau**2/r**2))-abs(x)*tau/r**2)
                   ks1=zsqrt(pp**2+1/Vs1q**2)
                   Coeff=calccoeffs_free_pp(pp,q)
                 uxinterbis=uxinterbis+aimag(ima*pp*Coeff(2)*ks1)
                 uzinterbis=uzinterbis+aimag(ks1**2*Coeff(2))
              end DO
                uxinter=uxinter+uxinterbis*acosh(q1/q0)*dzeta*ddt*src
                uzinter=uzinter+uzinterbis*acosh(q1/q0)*dzeta*ddt*src
             end DO
          end if
       end if
       if (t.gt.t0) then
          if (t-2.*tdelay.gt.t0) then 
             ddt=2*tdelay/Nint 
             tau0=t-2.*tdelay 
          else 
             ddt=(t-t0)/Nint 
             tau0=t0 
          end if 
          do k=1,Nint
             tau=tau0+(k-1)*ddt+ddt/2.
             src=source(t-tau)
             q0=sqrt(tau**2/R**2-1/Vs1**2);
             eta=0;
             dq=q0/Nqint;
             uxinterbis=0.;uzinterbis=0.
             DO l=1,Nqint
                q=sin(eta)*q0
                Vs1q=Vs1/sqrt(1+Vs1**2*q**2)
                pp=ima*tau*abs(x)/r**2+abs(z+h)/r*sqrt((tau/r)**2-1/Vs1q**2)
                ks1=zsqrt(pp**2+1/Vs1q**2)
                Coeff=calccoeffs_free_pp(pp,q)
                uxinterbis=uxinterbis-real(ima*pp*ks1*Coeff(2))
                 uzinterbis=uzinterbis-real(ks1**2*Coeff(2))
             eta=eta+deta  
          end do
          uxinter=uxinter+uxinterbis*deta*ddt*src
          uzinter=uzinter+uzinterbis*deta*ddt*src
       end do
    END if
          Ux(i,j)=Ux(i,j)+F*uxinter/(pi**2*R) 
          Uz(i,j)=Uz(i,j)+F*uzinter/(pi**2*R) 
  END DO
end DO
END SUBROUTINE sub_reflexss_free_pp
