!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

SUBROUTINE sub_reflexSP_ea(Nt_loc,J_array_loc)
  USE m_phys  
  USE m_num
  USE m_source
  USE m_sismo
  USE m_const
  IMPLICIT NONE 
#include "m_interface.h90"
  INTEGER I,J,K,L
  REAL*8 t0,x,y,ddt,src,t,tau,F,thead,thead2,dq,q,Vs1q,Vp1q,q1,uxinter,uzinter&
       &,uxinterbis,uzinterbis,tau0,q02
  COMPLEX*16::pp,Coeff(3),kf1,pp0,green,ks1,kp1

  INTEGER :: Nt_loc
  INTEGER, DIMENSION(Nt) :: J_array_loc
  INTEGER :: J_loc

  F=ampS
  vmax=MAX(MAX(Vp1,Vs1),V2)
  DO I=1,Nx
     x=X1+(I-1)*dx
     y=Y1+(I-1)*dy
     x=SQRT(x**2+y**2)
     t0=arrivaltime(h,x,-z,Vs1,Vp1)
     pp0=path(x,-z,h,Vs1,Vp1,t0)     
     DO J_loc=1,Nt_loc
        J = J_array_loc(J_loc)
        t=T1+(j-1)*dt
        uxinter=0.;uzinter=0.
        IF (ABS(aimag(pp0)).GT.1/Vmax) THEN
           !Computation of the head wave'
           thead=ABS(z)*SQRT(1/Vp1**2-1/vmax**2)+h*SQRT(1/Vs1**2-1/vmax**2)&
                &+ABS(x)/vmax;
           thead2=ABS(z)*SQRT(1/Vp1**2-1./Vmax**2)+h*SQRT(1./Vs1**2-1./Vmax**2)
           thead2=thead2+x**2/(h/SQRT(1./Vs1**2-1./Vmax**2)+ABS(z)/SQRT(1./Vp1&
                &**2-1./Vmax**2))
           IF((t.GT.thead).AND.(t-2.*tdelay.LT.t0)) THEN
              IF (t.GE.t0) THEN
                 IF (t-2.*tdelay.GT.thead) THEN 
                    ddt=(t0-t+2.*tdelay)/Nint 
                    tau0=t-2.*tdelay 
                 ELSE 
                    ddt=(t0-thead)/Nint 
                    tau0=thead 
                 END IF
              ELSE
                 IF (t-2.*tdelay.GT.thead) THEN 
                    tau0=t-2.*tdelay 
                    ddt=2.*tdelay/Nint 
                 ELSE 
                    ddt=(t-thead)/Nint 
                    tau0=thead 
                 END IF
              END IF
              DO  k=1,Nint
                 tau=tau0+(k-1)*ddt+ddt/2.
                 pp=ima*(tau-ABS(z)*SQRT(1./Vp1**2-1./Vmax**2)-h*SQRT(1./Vs1**2&
                      &-1./Vmax**2))/ABS(x)
                 q1=SQRT(aimag(pp)**2-1./Vmax**2)
                 dq=q1/Nqint
                 src=source(t-tau)
                 uxinterbis=0.;uzinterbis=0.
                 q=dq/2.
                 DO l=1,Nqint
                    Vs1q=Vs1/dsqrt(1+Vs1**2*q**2);
                    Vp1q=Vp1/dsqrt(1+Vp1**2*q**2);
                    pp=path(-ABS(x),-z,h,Vs1q,Vp1q,tau)    
                    pp=ima*aimag(pp)   
                    IF (AIMAG(pp)**2>1/Vs1q**2) THEN
                       ks1=ima*SQRT(-pp**2-1/Vs1q**2)
                    ELSE
                       ks1=SQRT(pp**2+1/Vs1q**2)
                    END IF
                    kp1=zsqrt(pp**2+1/Vp1q**2)
                    Coeff=calccoeffS_ea(pp,q)
                    green=derivee(-ABS(x),-z,h,Vs1q,Vp1q,pp)
                    uxinterbis=uxinterbis-real(ima*pp*Coeff(1)*green)
                    uzinterbis=uzinterbis-real(kp1*Coeff(1)*green)
                    q=q+dq
                 END DO
                 uxinter=uxinter+uxinterbis*dq*ddt*src
                 uzinter=uzinter+uzinterbis*dq*ddt*src
              END DO
           END IF
           IF((t.GE.t0).AND.(t-2.*tdelay.LT.thead2)) THEN
              IF (t.GE.thead2) THEN
                 IF (t-2.*tdelay.GT.t0) THEN 
                    tau0=t-2.*tdelay 
                    ddt=(thead2-tau0)/Nint 
                 ELSE 
                    tau0=t0
                    ddt=(thead2-tau0)/Nint 
                 END IF
              ELSE
                 IF (t-2.*tdelay.GT.t0) THEN  
                    tau0=t-2.*tdelay  
                    ddt=2.*tdelay/Nint  
                 ELSE  
                    ddt=(t-t0)/Nint  
                    tau0=t0 
                 END IF
              END IF
              q02=0.
              DO  k=1,Nint
                 tau=tau0+(k-1)*ddt+ddt/2.
                 src=source(t-tau)
                 q02=calcq02(q02,x,-z,h,Vs1,Vp1,Nq02,tau,t0)
                 pp=ima*(tau-ABS(z)*SQRT(1./Vp1**2-1./Vmax**2)-h*SQRT(1./Vs1**2&
                      &-1./Vmax**2))/ABS(x)
                 q1=SQRT(aimag(pp)**2-1./Vmax**2)
                 dq=(q1-q02)/Nqint
                 q=q02+dq/2.
                 uxinterbis=0.;uzinterbis=0.
                 DO l=1,Nqint
                    Vs1q=Vs1/dsqrt(1+Vs1**2*q**2);
                    Vp1q=Vp1/dsqrt(1+Vp1**2*q**2);
                    pp=path(-ABS(x),-z,h,Vs1q,Vp1q,tau)    
                    pp=ima*aimag(pp)     
                    kp1=zsqrt(pp**2+1/Vp1q**2)
                    IF (AIMAG(pp)**2>1/Vs1q**2+q**2) THEN
                       ks1=ima*SQRT(-pp**2-1/Vs1q**2)
                    ELSE
                       ks1=SQRT(pp**2+1/Vs1q**2)
                    END IF
                    kp1=zsqrt(pp**2+1/Vp1q**2)
                    Coeff=calccoeffS_ea(pp,q)
                    green=derivee(-ABS(x),-z,h,Vs1q,Vp1q,pp)
                    uxinterbis=uxinterbis-real(ima*pp*Coeff(1)*green)
                    uzinterbis=uzinterbis-real(kp1*Coeff(1)*green)
                    q=q+dq
                 END DO
                 uxinter=uxinter+uxinterbis*dq*ddt*src
                 uzinter=uzinter+uzinterbis*dq*ddt*src
              END DO
           END IF
        END IF
        IF (t.GT.t0) THEN
           IF (t-2.*tdelay.GT.t0) THEN
              ddt=2*tdelay/Nint
              tau0=t-2.*tdelay
           ELSE
              ddt=(t-t0)/Nint
              tau0=t0
           END IF
           q02=0.
           DO k=1,Nint
              tau=tau0+(k-1)*ddt+ddt/2.
              src=source(t-tau)
              q02=calcq02(q02,x,-z,h,Vs1,Vp1,Nq02,tau,t0)
              dq=q02/Nqint
              q=dq/2.
              uxinterbis=0.;uzinterbis=0.
              DO l=1,Nqint
                 Vs1q=Vs1/dsqrt(1+Vs1**2*q**2);
                 Vp1q=Vp1/dsqrt(1+Vp1**2*q**2);
                 pp=path(-ABS(x),-z,h,Vs1q,Vp1q,tau)   
                 kp1=zsqrt(pp**2+1/Vp1q**2)
                 ks1=zsqrt(pp**2+1/Vs1q**2)
                 Coeff=calccoeffS_ea(pp,q)
                 green=derivee(-ABS(x),-z,h,Vs1q,Vp1q,pp)
                 uxinterbis=uxinterbis-real(ima*pp*Coeff(1)*green)
                 uzinterbis=uzinterbis-real(kp1*Coeff(1)*green)
                 q=q+dq
              END DO
              uxinter=uxinter+uxinterbis*dq*ddt*src
              uzinter=uzinter+uzinterbis*dq*ddt*src
           END DO
        END IF
        Ux(i,j)=Ux(i,j)+F*uxinter/pi**2
        Uz(i,j)=Uz(i,j)+F*uzinter/pi**2
     END DO
  END DO
END SUBROUTINE sub_reflexSP_ea
