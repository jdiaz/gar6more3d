!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

SUBROUTINE sub_reflex_free_aa(Nt_loc,J_array_loc)
  Use m_phys  
  Use m_num
  Use m_source
  Use m_sismo
  Use m_const
  implicit none 
#include "m_interface.h90"
  integer I,J,K
  real*8 t0,r,x,y,ddt,src,t,tau,F,tau0,uxinter,uzinter,pinter

  integer :: Nt_loc
  integer, dimension(Nt) :: J_array_loc
  integer :: J_loc

  F=amp/V1**2
  DO I=1,Nx
     x=X1+(I-1)*dx
     y=Y1+(I-1)*dy
     x=sqrt(x**2+y**2)
     r=dsqrt(x**2+(z+h)**2)
     t0=r/V1
     DO J_loc=1,Nt_loc
        J = J_array_loc(J_loc)
       t=T1+(j-1)*dt
       if (t.gt.t0) then
          if (t-2.*tdelay .gt. t0) then
             ddt=2*tdelay/Nint 
             tau0=t-2.*tdelay 
          else 
             ddt=(t-t0)/Nint 
             tau0=t0 
          end if
             uxinter=0.;uzinter=0.;pinter=0.
          do k=1,Nint
             tau=tau0+(k-1)*ddt+ddt/2.
             src=source(t-tau)
             uxinter=uxinter+tau*src 
             uzinter=uzinter+tau*src
             pinter=pinter+src
          end do
             Ux(i,j)=Ux(i,j)+F*abs(x)*uxinter/(4*pi*r**3*rho1)*ddt 
             Uz(i,j)=Uz(i,j)-F*(z+h)*uzinter/(4*pi*r**3*rho1)*ddt 
             P(i,j)=P(i,j)-F*pinter/(4*pi*r)*ddt 
       end if
     END DO
  END DO
END SUBROUTINE sub_reflex_free_aa
