!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

SUBROUTINE sub_transmitPP_ee(Nt_loc,J_array_loc)
  Use m_phys  
  Use m_num
  Use m_source
  Use m_sismo
  Use m_const
  implicit none 
#include "m_interface.h90"
  integer I,J,K,L
  real*8 t0,x,y,ddt,src,t,tau,F,thead,thead2,dq,q,Vp1q,Vp2q,q1,uxinter,uzinter,uxinterbis,uzinterbis,tau0,q02
  complex*16::pp,Coeff(4),kp2,pp0,green

  integer :: Nt_loc
  integer, dimension(Nt) :: J_array_loc
  integer :: J_loc

  F=ampP
  vmax=max(Vs2,Vs1)
  DO I=1,Nx
     x=X1+(I-1)*dx
     y=Y1+(I-1)*dy
     x=sqrt(x**2+y**2)
     t0=arrivaltime(h,x,z,Vp1,Vp2)
     pp0=path(x,z,h,Vp1,Vp2,t0)     
     DO J_loc=1,Nt_loc
        J = J_array_loc(J_loc)
       t=T1+(j-1)*dt
       uxinter=0.;uzinter=0.
       if (abs(aimag(pp0)).gt.1/Vmax) then
          !Computation of the head wave'
          thead=abs(z)*sqrt(1/Vp2**2-1/vmax**2)+h*sqrt(1/Vp1**2-1/vmax**2)+abs(x)/vmax;
          thead2=abs(z)*sqrt(1/Vp2**2-1./Vmax**2)+h*sqrt(1./Vp1**2-1./Vmax**2)
          thead2=thead2+x**2/(h/sqrt(1./Vp1**2-1./Vmax**2)+abs(z)/sqrt(1./Vp2**2-1./Vmax**2))
          if((t.gt.thead).and.(t-2.*tdelay.lt.t0)) then
             if (t.ge.t0) then
                if (t-2.*tdelay.gt.thead) then 
                   ddt=(t0-t+2.*tdelay)/Nint 
                   tau0=t-2.*tdelay 
                else 
                   ddt=(t0-thead)/Nint 
                   tau0=thead 
                end if
             else
                if (t-2.*tdelay.gt.thead) then 
                   tau0=t-2.*tdelay 
                   ddt=2.*tdelay/Nint 
                else 
                   ddt=(t-thead)/Nint 
                   tau0=thead 
                end if 
             end if
             DO  k=1,Nint
                tau=tau0+(k-1)*ddt+ddt/2.
                pp=ima*(tau-abs(z)*SQRT(1./Vp2**2-1./Vmax**2)-h*SQRT(1./Vp1**2-1./Vmax**2))/abs(x)
                q1=sqrt(aimag(pp)**2-1./Vmax**2)
                dq=q1/Nqint
                src=source(t-tau)
                uxinterbis=0.;uzinterbis=0.
                q=dq/2.
                DO l=1,Nqint
                  Vp1q=Vp1/dsqrt(1+Vp1**2*q**2);
                  Vp2q=Vp2/dsqrt(1+Vp2**2*q**2);
                  pp=path(-abs(x),z,h,Vp1q,Vp2q,tau)  
                  pp=ima*aimag(pp)     
                  kp2=zsqrt(pp**2+1/Vp2q**2)
                  Coeff=calccoeffP_ee(pp,q)
                green=derivee(-abs(x),z,h,Vp1q,Vp2q,pp)
                uxinterbis=uxinterbis-real(ima*pp*Coeff(3)*green)
                uzinterbis=uzinterbis+real(kp2*Coeff(3)*green)
                q=q+dq
             END DO
             uxinter=uxinter+uxinterbis*dq*ddt*src
             uzinter=uzinter+uzinterbis*dq*ddt*src
             END DO
          end if
          if((t.ge.t0).and.(t-2.*tdelay.lt.thead2)) then
             if (t.ge.thead2) then
                if (t-2.*tdelay.gt.t0) then 
                   tau0=t-2.*tdelay 
                   ddt=(thead2-tau0)/Nint 
                else 
                   tau0=t0
                   ddt=(thead2-tau0)/Nint 
                end if 
             else
               if (t-2.*tdelay.gt.t0) then  
                   tau0=t-2.*tdelay  
                   ddt=2.*tdelay/Nint  
                else  
                   ddt=(t-t0)/Nint  
                   tau0=t0 
                end if  
             end if
             q02=0.
             DO  k=1,Nint
                tau=tau0+(k-1)*ddt+ddt/2.
                src=source(t-tau)
                q02=calcq02(q02,x,z,h,Vp1,Vp2,Nq02,tau,t0)
                pp=ima*(tau-abs(z)*SQRT(1./Vp2**2-1./Vmax**2)-h*SQRT(1./Vp1**2-1./Vmax**2))/abs(x)
                q1=sqrt(aimag(pp)**2-1./Vmax**2)
                dq=(q1-q02)/Nqint
                q=q02+dq/2.
                uxinterbis=0.;uzinterbis=0.
                DO l=1,Nqint
                  Vp1q=Vp1/dsqrt(1+Vp1**2*q**2);
                  Vp2q=Vp2/dsqrt(1+Vp2**2*q**2);
                  pp=path(-abs(x),z,h,Vp1q,Vp2q,tau)  
                  pp=ima*aimag(pp)     
                  kp2=zsqrt(pp**2+1/Vp2q**2)
                  Coeff=calccoeffP_ee(pp,q)
                green=derivee(-abs(x),z,h,Vp1q,Vp2q,pp)
                uxinterbis=uxinterbis-real(ima*pp*Coeff(3)*green)
                uzinterbis=uzinterbis+real(kp2*Coeff(3)*green)
                q=q+dq
             END DO
             uxinter=uxinter+uxinterbis*dq*ddt*src
             uzinter=uzinter+uzinterbis*dq*ddt*src
             END DO
         end if
      end if
       if (t.gt.t0) then
          if (t-2.*tdelay.gt.t0) then
             ddt=2*tdelay/Nint
             tau0=t-2.*tdelay
          else
             ddt=(t-t0)/Nint
             tau0=t0
          end if
         q02=0.
          do k=1,Nint
             tau=tau0+(k-1)*ddt+ddt/2.
             src=source(t-tau)
             q02=calcq02(q02,x,z,h,Vp1,Vp2,Nq02,tau,t0)
             dq=q02/Nqint
             q=dq/2.
             uxinterbis=0.;uzinterbis=0.
             DO l=1,Nqint
               Vp1q=Vp1/dsqrt(1+Vp1**2*q**2);
               Vp2q=Vp2/dsqrt(1+Vp2**2*q**2);
               pp=path(-abs(x),z,h,Vp1q,Vp2q,tau)   
               kp2=zsqrt(pp**2+1/Vp2q**2)
               Coeff=calccoeffP_ee(pp,q)
             green=derivee(-abs(x),z,h,Vp1q,Vp2q,pp)
             uxinterbis=uxinterbis-real(ima*pp*Coeff(3)*green)
             uzinterbis=uzinterbis+real(kp2*Coeff(3)*green)
             q=q+dq
          end do
          uxinter=uxinter+uxinterbis*dq*ddt*src
          uzinter=uzinter+uzinterbis*dq*ddt*src
       end do
       end if
       Ux(i,j)=Ux(i,j)+F*uxinter/pi**2
    Uz(i,j)=Uz(i,j)+F*uzinter/pi**2
     END DO
  END DO
END SUBROUTINE sub_transmitPP_ee
