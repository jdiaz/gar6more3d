!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

SUBROUTINE sub_reflexPS_ee(Nt_loc,J_array_loc)
  Use m_phys  
  Use m_num
  Use m_source
  Use m_sismo
  Use m_const
  implicit none 
#include "m_interface.h90"
  integer I,J,K,L
  real*8 t0,x,y,ddt,src,t,tau,F,thead,thead2,dq,q,Vs1q,Vp1q,q1,uxinter,uzinter,uxinterbis,uzinterbis,tau0,q02
  complex*16::pp,Coeff(4),kf1,pp0,green,ks1,kp1

  integer :: Nt_loc
  integer, dimension(Nt) :: J_array_loc
  integer :: J_loc

  F=ampP
  vmax=max(max(Vp1,Vs1),max(Vp2,Vs2))
  DO I=1,Nx
     x=X1+(I-1)*dx
     y=Y1+(I-1)*dy
     x=sqrt(x**2+y**2)
     t0=arrivaltime(h,x,-z,Vp1,Vs1)
     pp0=path(x,-z,h,Vp1,Vs1,t0)     
     DO J_loc=1,Nt_loc
        J = J_array_loc(J_loc)
       t=T1+(j-1)*dt
       uxinter=0.;uzinter=0.
       if (abs(aimag(pp0)).gt.1/Vmax) then
          !Computation of the head wave'
          thead=abs(z)*sqrt(1/Vs1**2-1/vmax**2)+h*sqrt(1/Vp1**2-1/vmax**2)+abs(x)/vmax;
          thead2=abs(z)*sqrt(1/Vs1**2-1./Vmax**2)+h*sqrt(1./Vp1**2-1./Vmax**2)
          thead2=thead2+x**2/(h/sqrt(1./Vp1**2-1./Vmax**2)+abs(z)/sqrt(1./Vs1**2-1./Vmax**2))
          if((t.gt.thead).and.(t-2.*tdelay.lt.t0)) then
             if (t.ge.t0) then
                if (t-2.*tdelay.gt.thead) then 
                   ddt=(t0-t+2.*tdelay)/Nint 
                   tau0=t-2.*tdelay 
                else 
                   ddt=(t0-thead)/Nint 
                   tau0=thead 
                end if
             else
                if (t-2.*tdelay.gt.thead) then 
                   tau0=t-2.*tdelay 
                   ddt=2.*tdelay/Nint 
                else 
                   ddt=(t-thead)/Nint 
                   tau0=thead 
                end if 
             end if
             DO  k=1,Nint
                tau=tau0+(k-1)*ddt+ddt/2.
                pp=ima*(tau-abs(z)*sqrt(1./Vs1**2-1./Vmax**2)-h*sqrt(1./Vp1**2-1./Vmax**2))/abs(x)
                q1=sqrt(aimag(pp)**2-1./Vmax**2)
                dq=q1/Nqint
                src=source(t-tau)
                uxinterbis=0.;uzinterbis=0.
                q=dq/2.
                DO l=1,Nqint
                   Vp1q=Vp1/dsqrt(1+Vp1**2*q**2);
                   Vs1q=Vs1/dsqrt(1+Vs1**2*q**2);
                   pp=path(-abs(x),-z,h,Vp1q,Vs1q,tau)    
                   pp=ima*aimag(pp)   
                   if (aimag(pp)**2>1/Vp1q**2) then
                      kp1=ima*sqrt(-pp**2-1/Vp1q**2)
                   else
                      kp1=sqrt(pp**2+1/Vp1q**2)
                   end if
                   ks1=zsqrt(pp**2+1/Vs1q**2)
                   Coeff=calccoeffP_ee(pp,q)
                   green=derivee(-abs(x),-z,h,Vp1q,Vs1q,pp)
                   uxinterbis=uxinterbis+real(ima*pp*ks1*Coeff(2)*green)
                   uzinterbis=uzinterbis+real((pp**2+q**2)*Coeff(2)*green)
             q=q+dq
             END DO
            uxinter=uxinter+uxinterbis*dq*ddt*src
             uzinter=uzinter+uzinterbis*dq*ddt*src
             END DO
          end if
          if((t.ge.t0).and.(t-2.*tdelay.lt.thead2)) then
             if (t.ge.thead2) then
                if (t-2.*tdelay.gt.t0) then 
                   tau0=t-2.*tdelay 
                   ddt=(thead2-tau0)/Nint 
                else 
                   tau0=t0
                   ddt=(thead2-tau0)/Nint 
                end if 
             else
               if (t-2.*tdelay.gt.t0) then  
                   tau0=t-2.*tdelay  
                   ddt=2.*tdelay/Nint  
                else  
                   ddt=(t-t0)/Nint  
                   tau0=t0 
                end if  
             end if
             q02=0.
            DO  k=1,Nint
                tau=tau0+(k-1)*ddt+ddt/2.
                src=source(t-tau)
                q02=calcq02(q02,x,-z,h,Vp1,Vs1,Nq02,tau,t0)
                pp=ima*(tau-abs(z)*sqrt(1./Vs1**2-1./Vmax**2)-h*sqrt(1./Vp1**2-1./Vmax**2))/abs(x)
                q1=sqrt(aimag(pp)**2-1./Vmax**2)
                dq=(q1-q02)/Nqint
                q=q02+dq/2.
                uxinterbis=0.;uzinterbis=0.
                DO l=1,Nqint
                   Vp1q=Vp1/dsqrt(1+Vp1**2*q**2);
                   Vs1q=Vs1/dsqrt(1+Vs1**2*q**2);
                   pp=path(-abs(x),-z,h,Vp1q,Vs1q,tau)    
                   pp=ima*aimag(pp)     
                   ks1=zsqrt(pp**2+1/Vs1q**2)
                   if (aimag(pp)**2>1/Vp1q**2+q**2) then
                      kp1=ima*sqrt(-pp**2-1/Vp1q**2)
                   else
                      kp1=sqrt(pp**2+1/Vp1q**2)
                   end if
                   ks1=zsqrt(pp**2+1/Vs1q**2)
                   Coeff=calccoeffP_ee(pp,q)
                   green=derivee(-abs(x),-z,h,Vp1q,Vs1q,pp)
                   uxinterbis=uxinterbis+real(ima*pp*ks1*Coeff(2)*green)
                   uzinterbis=uzinterbis+real((pp**2+q**2)*Coeff(2)*green)
             q=q+dq
             END DO
            uxinter=uxinter+uxinterbis*dq*ddt*src
             uzinter=uzinter+uzinterbis*dq*ddt*src
             END DO
          end if
       end if
       if (t.gt.t0) then
          if (t-2.*tdelay.gt.t0) then
             ddt=2*tdelay/Nint
             tau0=t-2.*tdelay
          else
             ddt=(t-t0)/Nint
             tau0=t0
          end if
          q02=0.
          do k=1,Nint
             tau=tau0+(k-1)*ddt+ddt/2.
             src=source(t-tau)
             q02=calcq02(q02,x,-z,h,Vp1,Vs1,Nq02,tau,t0)
             dq=q02/Nqint
             q=dq/2.
             uxinterbis=0.;uzinterbis=0.
             DO l=1,Nqint
                Vp1q=Vp1/dsqrt(1+Vp1**2*q**2);
                Vs1q=Vs1/dsqrt(1+Vs1**2*q**2);
                pp=path(-abs(x),-z,h,Vp1q,Vs1q,tau)   
                ks1=zsqrt(pp**2+1/Vs1q**2)
                kp1=zsqrt(pp**2+1/Vp1q**2)
                Coeff=calccoeffP_ee(pp,q)
                green=derivee(-abs(x),-z,h,Vp1q,Vs1q,pp)
                uxinterbis=uxinterbis+real(ima*pp*ks1*Coeff(2)*green)
                uzinterbis=uzinterbis+real((pp**2+q**2)*Coeff(2)*green)
             q=q+dq
          end do
          uxinter=uxinter+uxinterbis*dq*ddt*src
          uzinter=uzinter+uzinterbis*dq*ddt*src
       end do
       end if
       Ux(i,j)=Ux(i,j)+F*uxinter/pi**2
       Uz(i,j)=Uz(i,j)+F*uzinter/pi**2
     END DO
  END DO
END SUBROUTINE sub_reflexPS_ee
